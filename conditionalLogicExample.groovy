def addProFormaDataFromIssueArray(issueArray) {
    
    def result = 0
    for (issueNum = 0; issueNum < issueArray.size(); issueNum++){
        
        def issue = issueArray[issueNum]
        def issueData = get("/rest/api/2/issue/${issue}") 
                        .header('Content-Type', 'application/json') 
                        .asObject(Map).body 
        def issueFields = issueData.fields as Map
        def issueValue = issueFields.customfield_10053
        result += issueValue
        
    }
    
    return result
}

def saveProFormaDataToNewTask(projectKey, data) {
    
    def taskType = get('/rest/api/2/issuetype').asObject(List).body.find { it['name'] == 'Task' }['id']
    post('/rest/api/2/issue')
        .header('Content-Type', 'application/json')
        .body(
        [
                fields: [
                        summary    : 'Task Summary',
                        description: data, // insert data here
                        project    : [
                                key: projectKey
                        ],
                        issuetype  : [
                                id: taskType
                        ]
                ]
        ])
        .asString().body
}

def createMajorOutageTask(affectedUsers, threshold, projectKey) {
    
    // If affectedUsers is greated than the threshold, create a new issue in
    // projectKey project. 
    
    if (affectedUsers > threshold) {
        
        def taskType = get('/rest/api/2/issuetype').asObject(List).body.find { it['name'] == 'Task' }['id']
    post('/rest/api/2/issue')
        .header('Content-Type', 'application/json')
        .body(
        [
                fields: [
                        summary    : 'MAJOR OUTAGE',
                        description: affectedUsers.toString() + " Users Affected", 
                        project    : [
                                key: projectKey
                        ],
                        issuetype  : [
                                id: taskType
                        ]
                ]
        ])
        .asString().body
        
    }
    
}

def issueArray = ["JSD-84", "JSD-85"]
def data = addProFormaDataFromIssueArray(issueArray)

createMajorOutageTask(data, 1000, "JSD")