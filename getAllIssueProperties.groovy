def getAllIssueProperties(issueKey) {
    
    // Fetch the issue object from the key 
    def issue = get("/rest/api/2/issue/${issueKey}") 
        .header('Content-Type', 'application/json') 
        .asObject(Map).body 
    // Get all the fields from the issue as a Map 
    
    def fields = issue.fields as Map 
    return fields

}

def result = getAllIssueProperties("JSD-85")