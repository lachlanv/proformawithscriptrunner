// The issue key 
def issueKey = 'JSD-30' // Insert Issue Key
// Fetch the issue object from the key 
def issue = get("/rest/api/2/issue/${issueKey}") 
    .header('Content-Type', 'application/json') 
    .asObject(Map).body 
// Get all the fields from the issue as a Map 

def fields = issue.fields as Map 
def summaryField = fields.summary  // insert desired property. 
return summaryField