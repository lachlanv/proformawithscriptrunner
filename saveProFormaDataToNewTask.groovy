def addProFormaDataFromIssueArray(issueArray) {
    
    def result = 0
    for (issueNum = 0; issueNum < issueArray.size(); issueNum++){
        
        def issue = issueArray[issueNum]
        def issueData = get("/rest/api/2/issue/${issue}") 
                        .header('Content-Type', 'application/json') 
                        .asObject(Map).body 
        def issueFields = issueData.fields as Map
        def issueValue = issueFields.customfield_10053
        result += issueValue
        
    }
    
    return result
}

def saveProFormaDataToNewTask(projectKey, data) {
    
    def taskType = get('/rest/api/2/issuetype').asObject(List).body.find { it['name'] == 'Task' }['id']
    post('/rest/api/2/issue')
        .header('Content-Type', 'application/json')
        .body(
        [
                fields: [
                        summary    : 'Task Summary',
                        description: data,
                        project    : [
                                key: projectKey
                        ],
                        issuetype  : [
                                id: taskType
                        ]
                ]
        ])
        .asString().body
}

issueArray = ["JSD-84", "JSD-85"]
data = addProFormaDataFromIssueArray(issueArray)

saveProFormaDataToNewTask("JSD", data.toString()) // must convert numerical data to string to add to description field.