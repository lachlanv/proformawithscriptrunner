<h1> ProForma With ScriptRunner </h1>

This Repo provides a loose mashup of examples for working with ProForma and ScripRunner for Jira. 
They are not the most robust scripts, but they can serve as reference material for how to read data from ProForma and perform computation and actions in ScriptRunner.

To get ProForma data into ScriptRunner, these are the most important steps to follow.

1.  (Optional) Create a custom field in Jira of a type supported by ProForma (read more on that [here](https://thinktilt.atlassian.net/wiki/spaces/PF/pages/306315621/Supported+Jira+fields))
2.  Create a ProForma form via the Form Builder. 
3.  Link a ProForma field to a Jira field. 
4.  Create an issue with the ProForma form attached. In the desired ProForma Field, enter in data that will be easy to serch for later
5.  In ScriptRunner, use getAllIssueProperties.groovy to get a list of fields. 
6.  Find the data you entered in the issue data (which hopefully was made easily searchable). Cmd+F or Ctrl+F is your friend here
7.  Note the field name. It will likely look like customfield_10053 but will be slightly different. 
8.  Now you can access that data through ScriptRunner. Check addProFormaFormValue.groovy for a "Hello World" example of how to do this. 
9.  Once you have that data, you can use it however you like. Perform computation on it, save it to external sources through REST API calls etc. 