def addProFormaDataFromIssueArray(issueArray) {
    
    def result = 0
    for (issueNum = 0; issueNum < issueArray.size(); issueNum++){
        
        def issue = issueArray[issueNum]
        def issueData = get("/rest/api/2/issue/${issue}") 
                        .header('Content-Type', 'application/json') 
                        .asObject(Map).body 
        def issueFields = issueData.fields as Map
        def issueValue = issueFields.customfield_10053
        result += issueValue
        
    }
    
    return result
}