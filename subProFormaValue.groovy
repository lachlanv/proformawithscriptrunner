def subProFormaValue(issueKeyOne, issueKeyTwo) {
    
    // The issue key 
    def issueOneKey = issueKeyOne 
    // Fetch the issue object from the key 
    def issueOne = get("/rest/api/2/issue/${issueOneKey}") 
        .header('Content-Type', 'application/json') 
        .asObject(Map).body 
    
    def issueTwoKey = issueKeyTwo
    def issueTwo = get("/rest/api/2/issue/${issueTwoKey}") 
        .header('Content-Type', 'application/json') 
        .asObject(Map).body 
    
    // Get all the fields from the issue as a Map 
    
    def issueOneFields = issueOne.fields as Map 
    def issueTwoFields = issueTwo.fields as Map
    
    def issueOneAffectedUsers = issueOneFields.customfield_10053 // Get the custom field key by returning issue.fields as Map and scrolling through the result
    def issueTwoAffectedUsers = issueTwoFields.customfield_10053
    def result = issueOneAffectedUsers - issueTwoAffectedUsers
    
    return result
}

def result = subProFormaValue("JSD-85", "JSD-84")
return result
